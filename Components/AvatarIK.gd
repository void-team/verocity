@tool
extends Node3D


@export var live_preview : bool

@export_category("IK Targets")

@export var head_remote_transform : RemoteTransform3D

@export var left_hand_remote_transform : RemoteTransform3D

@export var right_hand_remote_transform : RemoteTransform3D




# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if Engine.is_editor_hint() && live_preview or !Engine.is_editor_hint():
		$LogicContainer/RenIKFootPlacement.interpolate_transforms(Engine.get_physics_interpolation_fraction())
		$LogicContainer/RenIK.update_ik()

func _physics_process(delta: float) -> void:
	if Engine.is_editor_hint() && live_preview or !Engine.is_editor_hint(): 
		$LogicContainer/RenIKFootPlacement.update_placement(get_physics_process_delta_time())
