extends Node
class_name PlayerConfiguration

enum PLAYER_TYPE {
	DESKTOP_OBSERVER = 0,
	DESKTOP_XR = 1,
	STANDALONE_XR = 2
}

var player_type : PLAYER_TYPE
