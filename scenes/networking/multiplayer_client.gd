extends Node
class_name MultiplayerClent


const SERVER_PORT = 18212

@export var server_ip : String


var peer = ENetMultiplayerPeer.new()

var queued : bool = false

signal client_connected(err)

var online : bool:
	set(value):
		online = value
		if value:
			if multiplayer:
				join_server(server_ip)

			else:
				queued = true


@rpc("any_peer", "call_local")
func player_type(target_id, current_id):
	if PlayerConfig.player_type == 0:
		rpc_id(target_id, "sync_player", 0, current_id)
	else:
		rpc_id(target_id, "sync_player", 1, current_id)
@rpc("any_peer", "call_local")
func sync_player(type : int, id : int):
	var player_scene
	if type == 0:
		player_scene = load("res://scenes/pc_observer_3d.tscn").instantiate()
	else:
		player_scene = load("res://scenes/player.tscn").instantiate()
	player_scene.player_id = id
	get_parent().get_parent().add_child(player_scene)
	
		

func join_server(ip):
	peer.create_client(ip, SERVER_PORT)
	get_tree().get_multiplayer().multiplayer_peer = peer
	get_parent().player_id = get_tree().get_multiplayer().multiplayer_peer.get_unique_id()
	set_multiplayer_authority(get_tree().get_multiplayer().multiplayer_peer.get_unique_id())
	
	client_connected.emit(OK)
	
	# Spawn already connected players.
	for id in multiplayer.get_peers():
		add_player(id)
		
func add_player(id: int):
	# var character = preload("res://scenes/players/remote_actor.tscn").instantiate()
	var character = preload("res://scenes/players/pc_observer_3d.tscn").instantiate()
	# Set player id.
	character.player_id = id
	
	get_parent().get_parent().add_child(character, true)

func _ready() -> void:
	if queued:
		join_server(server_ip)
		get_tree().get_multiplayer().multiplayer_peer = peer


	
