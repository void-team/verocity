extends Node
class_name MultiplayerServer


@export var players_node : Node3D

const SERVER_PORT = 18212

const MAX_CLIENTS = 5

var peer = ENetMultiplayerPeer.new()


var upnp = UPNP.new()

signal upnp_completed(error)

signal server_activation_failure()
signal server_online()

var upnp_active := false

var thread = Thread.new()
var this_should_be_pointless_but_its_needed = Thread.new()

var queued := false
var online : bool:
	set(value):
		online = value
		if value:
			if multiplayer:
				activate_server()
				thread.start(_upnp_setup.bind(SERVER_PORT))

			else:
				queued = true


func activate_upnp():
	thread.start(_upnp_setup.bind(SERVER_PORT))

func deactivate_upnp():
	upnp.delete_port_mapping(SERVER_PORT)
	upnp_active = false


func activate_server():
	peer.create_server(SERVER_PORT, MAX_CLIENTS)
	get_tree().get_multiplayer().multiplayer_peer = peer
	server_online.emit()



func _upnp_setup(server_port):
	# UPNP queries take some time.
	var err = upnp.discover()

	if err != OK:
		push_error(str(err))
		call_deferred("emit_signal", "upnp_completed", err)
		upnp_active = false
		return

	if upnp.get_gateway() and upnp.get_gateway().is_valid_gateway():
		upnp.add_port_mapping(server_port, server_port, ProjectSettings.get_setting("application/config/name"), "UDP")
		upnp.add_port_mapping(server_port, server_port, ProjectSettings.get_setting("application/config/name"), "TCP")
		call_deferred("emit_signal", "upnp_completed", OK)
		upnp_active = true

func _ready() -> void:
	players_node = get_parent().get_parent()
	if queued:
		activate_server()
		thread.start(_upnp_setup.bind(SERVER_PORT))



