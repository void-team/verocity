extends PanelContainer

## The Target Node for any and all networking stuff to be recieved
@export var networking_target : Node


var network_node : Node

@export var server_scene : PackedScene = preload("res://scenes/networking/multiplayer_server.tscn")
@export var client_scene : PackedScene = preload("res://scenes/networking/multiplayer_client.tscn")

func _ready() -> void:
	$MarginContainer/VBoxContainer/Killswitch.disabled = true




func _on_server_pressed() -> void:
	$MarginContainer/VBoxContainer/Killswitch.disabled = false
	$MarginContainer/VBoxContainer/Server.disabled = true
	$MarginContainer/VBoxContainer/Client.disabled = true
	var server = server_scene.instantiate()
	server.online = true
	server.connect("upnp_completed", server_online)
	networking_target.add_child(server)
	$MarginContainer/VBoxContainer/Label.text = "Network: Activating Server"
	

func _on_client_pressed() -> void:
	$MarginContainer/VBoxContainer/Killswitch.disabled = false
	$MarginContainer/VBoxContainer/Server.disabled = true
	$MarginContainer/VBoxContainer/Client.disabled = true
	var client = client_scene.instantiate()
	client.online = true
	client.server_ip = "localhost"
	client.connect("client_connected", client_online)
	networking_target.add_child(client)
	$MarginContainer/VBoxContainer/Label.text = "Network: Connecting Client"

func server_online(d):
	$MarginContainer/VBoxContainer/Label.text = "Network: Server"
	

func client_online(err):
	if err == OK:
		$MarginContainer/VBoxContainer/Label.text = "Network: Client"



