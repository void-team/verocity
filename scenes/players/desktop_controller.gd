extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: float = ProjectSettings.get_setting("physics/3d/default_gravity")

@export var camera_node : Node3D
@export var left_hand_node : Node3D
@export var right_hand_node : Node3D

var camera_offset : Vector3

var left_hand_offset : Vector3
var left_hand_offset_rot : Vector3

var right_hand_offset : Vector3
var right_hand_offset_rot : Vector3

func _ready() -> void:
	
	set_process(get_multiplayer_authority() == multiplayer.get_unique_id())
	camera_offset = global_position + camera_node.global_position
	
	left_hand_offset = global_position + left_hand_node.global_position
	left_hand_offset_rot = global_rotation + left_hand_node.global_rotation
	
	right_hand_offset = global_position + right_hand_node.global_position
	right_hand_offset = global_rotation + right_hand_node.global_rotation

func _physics_process(delta: float) -> void:
	if not is_multiplayer_authority(): return
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	# Get the input direction and handle the movement/deceleration.
	var input_dir := Input.get_vector("left", "right", "forwards", "backwards")
	var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()
	
	# Update the Targeted nodes.
	# I know there's a better method of doing this, but I can't understand Transform3Ds lmao
	
	
	camera_node.global_position = camera_offset + global_position
	camera_node.global_rotation = global_rotation
	
	left_hand_node.global_position = left_hand_offset + global_position
	left_hand_node.global_rotation = left_hand_offset_rot + global_rotation
	
	right_hand_node.global_position = right_hand_offset + global_position
	right_hand_node.global_rotation = right_hand_offset_rot + global_rotation
	
