extends Node3D

var player_id := 1:
	set(value):
		player_id = value
		set_multiplayer_authority(value)
		

func _ready() -> void:
	set_multiplayer_authority(player_id)
	# $MultiplayerSynchronizer.set_root_path(get_path())
