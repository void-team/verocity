extends Node3D
class_name DesktopPlayer

var player_id := 1:
	set(value):
		player_id = value
		set_multiplayer_authority(player_id)

func _enter_tree() -> void:
	set_multiplayer_authority(player_id)
