extends XROrigin3D
class_name XRPlayer3D

@export var networked := false

@export var desktop_observer := false


var player_id := 1:
	set(value):
		player_id = value
		set_multiplayer_authority(value)

func _enter_tree() -> void:
	set_multiplayer_authority(player_id)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	

	if not is_multiplayer_authority():
		current = false
		$XRCamera3D.current = false
	else:
		current = true
		$Camera.current = true
