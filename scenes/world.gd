extends Node3D
class_name Level3D

const SPAWN_RANDOM := 5.0

func _ready():
	# We only need to spawn players on the server.
	if not multiplayer.is_server():
		return

	multiplayer.peer_connected.connect(add_remote_player)
	multiplayer.peer_disconnected.connect(del_player)


func _exit_tree():
	if not multiplayer.is_server():
		return
	multiplayer.peer_connected.disconnect(add_remote_player)
	multiplayer.peer_disconnected.disconnect(del_player)



func add_remote_player(id: int):
	# var character = preload("res://scenes/players/remote_actor.tscn").instantiate()
	var character = preload("res://scenes/players/pc_observer_3d.tscn").instantiate()
	# Set player id.
	character.player_id = id
	
	$Players.add_child(character, true)

func add_player(player: Node3D):
	$Players.add_child(player, true)

func del_player(id: int):
	if not $Players.has_node(str(id)):
		return
	$Players.get_node(str(id)).queue_free()
