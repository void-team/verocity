extends Node

@export var level_file : PackedScene

@export var xr_player_file : PackedScene
@export var desktop_observer_file : PackedScene

var level : Level3D

var player : Node3D

func _ready() -> void:
	
	
	var xr_interface = XRServer.find_interface('OpenXR')
	
	if xr_interface and xr_interface.is_initialized():
		pass
	else:
		xr_interface = null
	
	
	
	level = level_file.instantiate()
	if xr_interface:
		player = xr_player_file.instantiate()
		level.add_player(player)
		PlayerConfig.player_type = PlayerConfig.PLAYER_TYPE.STANDALONE_XR
	else:
		player = desktop_observer_file.instantiate()
		level.add_player(player)
		get_tree().root.call_deferred("add_child", level)
		PlayerConfig.player_type = PlayerConfig.PLAYER_TYPE.DESKTOP_OBSERVER
		get_node("/root/Init").queue_free()

func _on_start_xr_xr_started() -> void:
	get_tree().root.call_deferred("add_child", level)
	get_node("/root/Init").queue_free()

